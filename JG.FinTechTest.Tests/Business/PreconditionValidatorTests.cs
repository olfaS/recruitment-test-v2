﻿using JG.FinTechTest.Business;
using NUnit.Framework;

namespace JG.FinTechTest.Tests.Business
{
    [TestFixture]
    public class PreconditionValidatorTests
    {
        private IPreconditionValidator _preconditionValidator;
        [SetUp]
        public void SetUp()
        {
            _preconditionValidator = new PreconditionValidator();
        }

        [TestCase(55)]
        [TestCase(100000)]
        [TestCase(2)]
        [TestCase(1234)]
        public void ShouldReturnOkWhenAmountMatchesRequirements(double amount)
        {
            var validationResult = _preconditionValidator.Validate(amount);
            Assert.IsTrue(validationResult.Success);
        } 
        [Test]
        public void ShouldReturnFailWhenAmountLessThan2()
        {
            var amount = 1;
            var validationResult = _preconditionValidator.Validate(amount);
            Assert.IsFalse(validationResult.Success);
        }
        
        [Test]
        public void ShouldReturnFailWhenAmountMoreThan100000()
        {
            var amount = 100001;
            var validationResult = _preconditionValidator.Validate(amount);
            Assert.IsFalse(validationResult.Success);
        }
    }
}