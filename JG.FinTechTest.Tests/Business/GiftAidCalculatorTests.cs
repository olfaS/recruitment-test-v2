﻿using JG.FinTechTest.Business;
using NUnit.Framework;

namespace JG.FinTechTest.Tests.Business
{
    [TestFixture]
    public class GiftAidCalculatorTests
    {
        private ICalculator _calculator;
        private ITaxValue _taxValue;

        [SetUp]
        public void SetUp()
        {
            _taxValue = new TaxValue(20);
            _calculator = new GiftAidCalculator(_taxValue);
        }

        [TestCase(100, 25)]
        [TestCase(200, 50)]
        [TestCase(0, 0)]
        public void ShouldApplyFormulaCorrectly(double donationAmount, double expectedGiftAid)
        {
            var calculatedGiftAid = _calculator.CalculateAmount(donationAmount);
            Assert.AreEqual(expectedGiftAid, calculatedGiftAid);
        }
    }
}