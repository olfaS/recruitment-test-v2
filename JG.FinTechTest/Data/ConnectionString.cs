﻿namespace JG.FinTechTest.Data
{
    public class ConnectionString : IConnectionString
    {
        public ConnectionString(string value)
        {
            Value = value;
        }

        public string Value { get; }
    }
}