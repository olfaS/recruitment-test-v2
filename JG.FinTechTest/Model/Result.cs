﻿
namespace JG.FinTechTest.Model
{
    public class Result
    {
        private Result(bool success, string errorMessage)
        {
            Success = success;
            ErrorMessage = errorMessage;
        }

        public static Result Fail(string errorMessage)
        {
            return new Result(false, errorMessage);
        }

        public static Result Ok()
        {
            return new Result(true, null);
        }

        public bool Success { get; }
        public string ErrorMessage { get; }
    }
}