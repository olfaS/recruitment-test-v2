﻿namespace JG.FinTechTest.Business
{
    public interface ITaxValue
    {
        double Value { get; }
    }
}