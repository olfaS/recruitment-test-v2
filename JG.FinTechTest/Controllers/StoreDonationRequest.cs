﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace JG.FinTechTest.Controllers
{
    public class StoreDonationRequest
    {
        [Required] public string Name { get; set; }
        [Required] public string PostCode { get; set; }
        [Required] public double Amount { get; set; }
    }
}