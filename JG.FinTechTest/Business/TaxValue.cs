﻿namespace JG.FinTechTest.Business
{
    public class TaxValue : ITaxValue
    {
        public TaxValue(int value)
        {
            Value = value;
        }

        public double Value { get; }
    }
}