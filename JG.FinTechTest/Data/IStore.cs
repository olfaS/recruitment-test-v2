﻿using JG.FinTechTest.Model;

namespace JG.FinTechTest.Data
{
    public interface IStore
    {
        (Result result, int id) PersistDonation(string name, string postCode, double donationAmount);
    }
}