﻿using JG.FinTechTest.Business;
using JG.FinTechTest.Controllers;
using JG.FinTechTest.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSubstitute;
using NUnit.Framework;

namespace JG.FinTechTest.Tests.Controllers
{
    [TestFixture]
    public class GiftAidControllerTests
    {
        private GiftAidController _controller;
        private ICalculator _giftAidCalculator;
        private IPreconditionValidator _preconditionValidator;

        [SetUp]
        public void SetUp()
        {
            _giftAidCalculator = Substitute.For<ICalculator>();
            _preconditionValidator = Substitute.For<IPreconditionValidator>();
            _controller = new GiftAidController(_giftAidCalculator, _preconditionValidator );
        }

        [Test]
        public void ShouldReturnOkWhenGetCalledCorrectly()
        {
            _giftAidCalculator.CalculateAmount(42).Returns(5);
            _preconditionValidator.Validate(42).Returns(Result.Ok());
            var result = _controller.GiftAid(42);
            Assert.That(result, Is.TypeOf<OkObjectResult>());
            var okResult = (OkObjectResult)result;
            var expected = new GiftAidResponse(42, 5);
            var expectedJson = JsonConvert.SerializeObject(expected, Formatting.Indented);
            Assert.AreEqual(expectedJson, okResult.Value);
        }
        
        [Test]
        public void ShouldReturnBadRequestWhenGetCalledWithNonValidParams()
        {
            _giftAidCalculator.CalculateAmount(42).Returns(5);
            var message = "error";
            _preconditionValidator.Validate(42).Returns(Result.Fail(message));
            var result = _controller.GiftAid(42);
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());
            var badRequestResult = (BadRequestObjectResult)result;
            Assert.AreEqual(message, badRequestResult.Value);
        }
        
    }
}