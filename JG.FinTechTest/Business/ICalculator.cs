﻿namespace JG.FinTechTest.Business
{
    public interface ICalculator
    {
        ITaxValue TaxRate { get; }
        double CalculateAmount(double donationAmount);
    }
}