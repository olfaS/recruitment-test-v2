﻿using JG.FinTechTest.Data;
using LiteDB;
using NUnit.Framework;
using ConnectionString = JG.FinTechTest.Data.ConnectionString;

namespace JG.FinTechTest.Tests.Data
{
    [TestFixture]
    public class StoreTests
    {
        private IStore _store;
        private readonly IConnectionString _connectionString = new ConnectionString(@"test.db");

        [SetUp]
        public void SetUp()
        {
            _store = new Store(_connectionString);
        }

        [Test]
        public void ShouldInsertNewDonationAndIncrementIndex()
        {
            var initialCount = GetCount();
            var name = "toto";
            var postCode = "007";
            var amount = 123;
            var (result, id) = _store.PersistDonation(name, postCode, amount);

            var newCount = GetCount();
            var latestDonation = GetDonation(id);
            Assert.IsTrue(result.Success);
            Assert.AreEqual(newCount, initialCount + 1);
            Assert.AreEqual(latestDonation.Name, name);
            Assert.AreEqual(latestDonation.PostCode, postCode);
            Assert.AreEqual(latestDonation.DonationAmount, amount);
        }

        private int GetCount()
        {
            using (var dbContext = new LiteDatabase(_connectionString.Value))
            {
                var donations = dbContext.GetCollection<Donation>("donations");
                return donations.Count();
            }
        }

        private Donation GetDonation(int id)
        {
            using (var dbContext = new LiteDatabase(_connectionString.Value))
            {
                var donations = dbContext.GetCollection<Donation>("donations");
                var count = donations.Count();
                return donations.FindById(id);
            }
        }
    }
}