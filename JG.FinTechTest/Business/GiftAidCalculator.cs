﻿namespace JG.FinTechTest.Business
{
    public class GiftAidCalculator : ICalculator
    {
        public GiftAidCalculator(ITaxValue taxRate)
        {
            TaxRate = taxRate;
        }

        public ITaxValue TaxRate { get; }

        public double CalculateAmount(double donationAmount)
        {
            return donationAmount * (TaxRate.Value / (100 - TaxRate.Value));
        }
    }
}