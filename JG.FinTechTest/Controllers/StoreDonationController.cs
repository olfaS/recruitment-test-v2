﻿using System.ComponentModel.DataAnnotations;
using JG.FinTechTest.Business;
using JG.FinTechTest.Data;
using JG.FinTechTest.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace JG.FinTechTest.Controllers
{
    [Route("api/storedonation")]
    [ApiController]
    public class StoreDonationController : ControllerBase
    {
        private readonly IStore _store;
        private IPreconditionValidator _preconditionValidator;
        private readonly ICalculator _giftAidCalculator;

        public StoreDonationController(IStore store, ICalculator giftAidCalculator, IPreconditionValidator preconditionValidator)
        {
            _store = store;
            _giftAidCalculator = giftAidCalculator;
            this._preconditionValidator = preconditionValidator;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult StoreDonation([FromBody] StoreDonationRequest request)
        {
            var validationResult = _preconditionValidator.Validate(request.Amount);
            if(!validationResult.Success)
                return new BadRequestObjectResult(validationResult.ErrorMessage);
            var persistenceResult = _store.PersistDonation(request.Name, request.PostCode, request.Amount);
            if (!persistenceResult.result.Success)
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            var giftAidAmount = _giftAidCalculator.CalculateAmount(request.Amount);
            var response = new StoreDonationResponse(persistenceResult.id, giftAidAmount);
            var json = JsonConvert.SerializeObject(response, Formatting.Indented);
            return Ok(json);
        }
    }
}