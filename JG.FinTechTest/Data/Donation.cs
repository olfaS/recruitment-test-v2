﻿namespace JG.FinTechTest.Data
{
    public class Donation
    {
        public Donation(string name, string postCode, double donationAmount)
        {
            Name = name;
            PostCode = postCode;
            DonationAmount = donationAmount;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string PostCode { get; set; }
        public double DonationAmount { get; set; }
    }
}