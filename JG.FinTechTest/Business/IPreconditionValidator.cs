﻿using JG.FinTechTest.Model;

namespace JG.FinTechTest.Business
{
    public interface IPreconditionValidator
    {
        Result Validate(double amount);
    }
}