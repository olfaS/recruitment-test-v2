﻿using System;
using JG.FinTechTest.Model;
using LiteDB;

namespace JG.FinTechTest.Data
{
    public class Store : IStore
    {
        private readonly IConnectionString _connectionString;

        public Store(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public (Result result, int id) PersistDonation(string name, string postCode, double donationAmount)
        {
            var newDonation = new Donation(name, postCode, donationAmount);
            using (var dbContext = new LiteDatabase(_connectionString.Value))
            {
                try
                {
                    var donations = dbContext.GetCollection<Donation>("donations");
                    donations.Insert(newDonation);
                }
                catch (Exception exception)
                {
                    return (Result.Fail($"Couldn't insert into database {exception.Message}"), 0);
                }
            }

            return (Result.Ok(), newDonation.Id);
        }
    }
}