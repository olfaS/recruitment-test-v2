﻿namespace JG.FinTechTest.Model
{
    public class StoreDonationResponse
    {
        public StoreDonationResponse(int declarationId, double giftAidAmount)
        {
            DeclarationId = declarationId;
            GiftAidAmount = giftAidAmount;
        }

        public int DeclarationId { get; }
        public double GiftAidAmount { get; }
    }
}