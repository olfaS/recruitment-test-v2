﻿namespace JG.FinTechTest.Data
{
    public interface IConnectionString
    {
        string Value { get; }
    }
}