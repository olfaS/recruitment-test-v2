﻿using System.ComponentModel.DataAnnotations;
using JG.FinTechTest.Business;
using JG.FinTechTest.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace JG.FinTechTest.Controllers
{
    /// <summary>
    /// summary: Get the amount of gift aid reclaimable for donation amount
    /// </summary>
    /// <param name="amount">a required Number representing the amount of gift aid.</param>
    /// <returns>A JSON.</returns>
    [Route("api/giftaid")]
    [ApiController]
    public class GiftAidController : ControllerBase
    {
        private readonly ICalculator _giftAidCalculator;
        private readonly IPreconditionValidator _preconditionValidator;

        public GiftAidController(ICalculator giftAidCalculator, IPreconditionValidator preconditionValidator)
        {
            _giftAidCalculator = giftAidCalculator;
            _preconditionValidator = preconditionValidator;
        }

        [HttpGet]
        public IActionResult GiftAid([Required] double amount)
        {
            var validationResult = _preconditionValidator.Validate(amount);
            if (!validationResult.Success)
                return new BadRequestObjectResult(validationResult.ErrorMessage);
            var calculatedGiftAid = _giftAidCalculator.CalculateAmount(amount);
            var response = new GiftAidResponse(amount, calculatedGiftAid);
            var json = JsonConvert.SerializeObject(response, Formatting.Indented);
            return Ok(json);
        }
    }
}