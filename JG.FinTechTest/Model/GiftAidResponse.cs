﻿namespace JG.FinTechTest.Model
{
    public class GiftAidResponse
    {
        public GiftAidResponse(double giftAidAmount, double donationAmount)
        {
            this.GiftAidAmount = giftAidAmount;
            this.DonationAmount = donationAmount;
        }

        public double GiftAidAmount { get; }
        public double DonationAmount { get; }
    }
}