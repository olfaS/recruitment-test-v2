﻿using JG.FinTechTest.Model;

namespace JG.FinTechTest.Business
{
    public class PreconditionValidator : IPreconditionValidator
    {
        public Result Validate(double amount)
        {
            if (amount < 2)
                return Result.Fail("Minimum amount is 2");
            if (amount > 100000)
                return Result.Fail("Maximum amount is 100000");
            return Result.Ok();
        }
    }
}