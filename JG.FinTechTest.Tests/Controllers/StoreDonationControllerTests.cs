﻿using JG.FinTechTest.Business;
using JG.FinTechTest.Controllers;
using JG.FinTechTest.Data;
using JG.FinTechTest.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSubstitute;
using NUnit.Framework;

namespace JG.FinTechTest.Tests.Controllers
{
    [TestFixture]
    public class StoreDonationControllerTests
    {
        private IStore _store;
        private StoreDonationController _controller;
        private ICalculator _giftAidCalculator;
        private IPreconditionValidator _preconditionValidator;

        [SetUp]
        public void SetUp()
        {
            _store = Substitute.For<IStore>();
            _giftAidCalculator = Substitute.For<ICalculator>();
            _preconditionValidator = Substitute.For<IPreconditionValidator>();
            _controller = new StoreDonationController(_store, _giftAidCalculator, _preconditionValidator);
        }

        [Test]
        public void ShouldReturnOkWhenDonationStoredSuccessfully()
        {
            var name = "toto";
            var postCode = "007";
            var amount = 123;
            _preconditionValidator.Validate(amount).Returns(Result.Ok());
            _store.PersistDonation(name, postCode, amount).Returns((Result.Ok(), 42));
            double giftAidAmount = 55.5;
            _giftAidCalculator.CalculateAmount(amount).Returns(giftAidAmount);
            var result = _controller.StoreDonation(new StoreDonationRequest(){Name = name, Amount = amount, PostCode = postCode});
            Assert.That(result, Is.TypeOf<OkObjectResult>());

            var okResult = (OkObjectResult) result;
            var expected = new StoreDonationResponse(42, giftAidAmount);
            var expectedJson = JsonConvert.SerializeObject(expected, Formatting.Indented);
            Assert.AreEqual(expectedJson, okResult.Value);
        }
        
        
        [Test]
        public void ShouldReturnBadRequestWhenValidationFails()
        {
            var name = "toto";
            var postCode = "007";
            var amount = 123;
            var errorMessage = "Error";
            _preconditionValidator.Validate(amount).Returns(Result.Fail(errorMessage));
            var result = _controller.StoreDonation(new StoreDonationRequest(){Name = name, Amount = amount, PostCode = postCode});
            Assert.That(result, Is.TypeOf<BadRequestObjectResult>());
            var badRequestResult = (BadRequestObjectResult) result;
            Assert.AreEqual(errorMessage, badRequestResult.Value);
        }
        
        [Test]
        public void ShouldReturnInternalErrorWhenDonationNotStoredSuccessfully()
        {
            var name = "toto";
            var postCode = "007";
            var amount = 123; 
            _preconditionValidator.Validate(amount).Returns(Result.Ok());
            _store.PersistDonation(name, postCode, amount).Returns((Result.Fail("internal error"), 0));
            var result = _controller.StoreDonation(new StoreDonationRequest(){Name = name, Amount = amount, PostCode = postCode});
            Assert.That(result, Is.TypeOf<StatusCodeResult>());
            var okResult = (StatusCodeResult) result;
            Assert.AreEqual(StatusCodes.Status500InternalServerError, okResult.StatusCode);
        }

    }
}